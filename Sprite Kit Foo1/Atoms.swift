//  Atoms.swift -- built from Parts C2018PAK

import SceneKit

 /// An Atom is where computations are performed:
class Atom : Part {

	var ports : [String : Port] = [:]

	 /// p = primary, c = always create, d = down
	func createPorts()->[String:String]	{		return ["P":"pcd"]			}

	override init(_ parameters_: Dictionary<String,Any>?) {
		super.init(parameters_)

		 /// Create PORTS
		for (portName, portProps) in createPorts() {
			let portId 			= Port(["props":portProps])
			ports[portName] 	= portId
			self.addChild(portId)
			portId.name 		= portName
		}
	}

//	func rePosition() {
//		var myBboxMin : SCNVector3 	= SCNVector3Zero
//		var myBboxMax : SCNVector3	= SCNVector3Zero
//
//		for childNode_ in childNodes {
//			repositionQ(childNode, )
//	}
	 /// An Atom's Ports get positioned here
	func rePositionQ(port:Port, inView:View) {
		if port.name == "P" {		//// P: Primary
			inView.position = SCNVector3Zero		//suggestedPositionOfSpot
		}
	}


}

 /// A Port is where a Link is connected to an Atom:
class Port : Part {
	override init(_ parameters_: Dictionary<String,Any>?) {
		super.init(parameters_)
		geometryHint = SCNCylinder(radius:1, height:0.5)
	}
}

 /// An Ago is an Atom with a time delay. It has 2 Ports
class Ago : Atom {

	override func createPorts()->[String:String]	{
		var ports = super.createPorts()					//updateValue
		ports["S"]				= "pc "
		return ports
	}

	override init(_ parameters_: Dictionary<String,Any>?) {
		super.init(parameters_)
	}

	override func rePositionQ(port:Port, inView:View) {
		if port.name == "S" {		//// P: Primary
			inView.position = SCNVector3(x:0, y:2, z:0)
		}
		super.rePositionQ(port:port, inView: inView)
	}
}
