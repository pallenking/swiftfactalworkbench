//
//  GameViewController.swift
//  Sprite Kit Foo1
//
//  Created by Allen King on 12/10/17.
//  Copyright © 2017 Allen King. All rights reserved.
//

import UIKit
import SceneKit

class GameViewController: UIViewController {

	let simulator = Simulator("1Ago")		// build the world first, for now
	
	override func loadView() {

		  /// View from Model, a HaveNWant network
		 // create a scene view with an empty scene
		let scene 				= SCNScene()
		let sceneView 			= SCNView(frame: CGRect(x:0, y:0, width:300, height:300))
		sceneView.scene 		= scene
		sceneView.autoenablesDefaultLighting = true		// default lighting

		 // a camera
		let cameraNode 			= SCNNode()
		cameraNode.camera 		= SCNCamera()
		cameraNode.position 	= SCNVector3(x:0, y:5, z:15)
		scene.rootNode.addChildNode(cameraNode)

		 // Build Views from Root				+  +  +  +
		let rootView			= simulator.root.reView()
		scene.rootNode.addChildNode(rootView)	//	+  +  +  +

		rootView.rePosition()
		print("simulator.root:\n", simulator.root.pp())

		view = sceneView // Set the view property to the sceneView created here.
	}

    override func viewDidLoad() {
        super.viewDidLoad()
  }

	override var shouldAutorotate: Bool {
		return true
	}

 // Brian: where should this go?
//	override func mouseDown(theEvent: NSEvent)
//	{
//		var location = self.convertPoint(theEvent.locationInWindow, fromView: nil)
//		let hitResults = sceneView.hitTest(location, options: nil)
//		if hitResults?.count > 0 {
//			let result = hitResults![0] as! SCNHitTestResult
//			let node = result.node
//			//node.removeFromParentNode()
//		}
//	}

	override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
		if UIDevice.current.userInterfaceIdiom == .phone {
			return .allButUpsideDown
		} else {
			return .all
		}
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Release any cached data, images, etc that aren't in use.
	}

	override var prefersStatusBarHidden: Bool {
		return true
	}
}
