//  Simulator.swift -- Simulate the time evolution behavior of a Net C2018PAK

import SceneKit

class Simulator 	: NSObject {
	var root :Part		= Part([:])

	 /// Time Behavior:
	let logEvents 		= false
	let logTime 		= false
	let timeNow			= Double.nan
	let globalDagDirDown = false
	let logNumber		= Int(1)
	var lastTime : Double?	// = Double.nan;

	init(_ kind: String) {
		super.init()

		 /// Get a Network, insatll as root.  Simple ones for now
		if kind == "debug1" {
			// create a scene view with an empty scene
			for _ in 1...5 {
				let part1 = Part([:])
				root.addChild(part1)
				part1.geometryHint 		= SCNBox(width:0.75, height:1.5, length:1.5, chamferRadius: 0)
			}
			//root.children[0].geometry = SCNSphere(radius:1.5)//SCNCylinder(radius:1, height:5)//SCNPyramid(width:0.6, height:0.5, length:0.4)
		}
		else if kind == "1Atom" {
			let part = Atom([:])		// Atoms have Ports
			root.addChild(part)
		}
		else if kind == "1Ago" {
			let part = Ago([:])			// An Atom with 2 Ports
			root.addChild(part)
		}
		print(root.pp())
	}


	/// Common Functions (not yet needed):
	
//	func logEvent(target:Part, format:String, _ args:CVarArg...) {
//		if logEvents == false {
//			return														}
//
//		var str = String(format:format, arguments:args)
//
//
//		 // print if it has changed
//		let time = timeNow;
//		if lastTime != time && logTime {
//			let t2 = time-lastTime!
//			let delta = (lastTime != nil) ? "": fmt("+%.3f; ", t2)
//			print(fmt("T=%.3f %@: - - - - - - - - - - - - - - - - - - - - %@\n", time,
//								globalDagDirDown ? "DOWN": "UP  ", delta));
//			lastTime = time;
//		}
//
//		   // format special semantics of simulantion logs:
//		  //  leading '\n'     -- log IVal has extra leading  '\n' before message instead
//		 //  trailing '\n'    -- log IVal has extra trailing '\n' after  message instead
//		while str.count>0 && str.hasPrefix("\n") {
//			str.removeFirst()													}
//
//		 // format printout of self.name:
//		let nam = true ? target.fullName(): target.name
//		print(fmt("%-4d %@   %@", logNumber, nam, str))		//=== log number
//	}
}

//func runTime() -> Double {								// time since first call here
//	var time:timeval?
//	gettimeofday(&time!, nil) // this is the method expansion before filling in any data
//	return Double(time!.tv_sec) * 1000 + Double(time!.tv_usec) / 1000
//}

func fmt(_ format:String, _ args:CVarArg...) -> String {
	return  String(format:format, arguments:args)
}

func panic(format:String, args:CVarArg...) {
	let msg 					= String(format:format, arguments:args)
	print("\n\n--------------\n", msg, "\n\n--------------\n")
	raise(SIGINT)
}
func assert(_ truthValue:Bool, format:String, args:CVarArg...) {
	if truthValue {
		let msg 				= String(format:format, arguments:args)
		print("\n\n--------------\n", msg, "\n\n--------------\n")
		raise(SIGINT)
	}
}

