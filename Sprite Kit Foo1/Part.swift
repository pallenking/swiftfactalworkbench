// Part.swift -- base class for Factal Workbench TRIAL C2017PAK

import SceneKit
var partNo = 1

class Part : NSObject {		// SCNNode // NSObject

	var parameters			: [String:Any]
	var name				: String

	var children					= [Part]()
	var parent				: Part?
	var simulator			: Simulator?

	var geometryHint		: SCNGeometry?

	init(_ parameters_: Dictionary<String,Any>?) {
		name						= fmt("P%d", partNo)
		partNo						= partNo + 1
		parameters					= parameters_!
		super.init()
	}
	func addChild(_ part:Part) {
		children.append(part)
		part.parent					= self
	}
//	func parameter(key:String)->Any	{
//		return self.parameters[key]!
//	}

	 // remove parameter from self.parameters before returning it
//	func takeParameter(key:String) -> Any? {
//		if let rv = self.parameters[key] {
//			self.parameters[key] = nil		// remove object from parameters
//			return rv;
//		}
//		return nil;
//	}

	///////////////////////////////
	func fullName() -> String {
		let parentsFullName 		= self.parent?.fullName() ?? ""
		return parentsFullName + "/" + name
	}

	func reView() -> View {
		let rv						= View(part:self)//)
		rv.geometry					= self.geometryHint

		for part in children {
			let newView = part.reView() 	// get a view for it
			rv.addChildNode(newView)
		}
		return rv;
	}

	 /// Pretty Print
	func pp() -> String {
		var rv = fmt("Part named %@\n", fullName())
		for child in children {
			rv = rv + child.pp()// + "\n"
		}
		return rv
	}
}
