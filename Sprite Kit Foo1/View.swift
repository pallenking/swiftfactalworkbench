//  View.swift -- a tree of Views per part C2013PAK

//import UIKit
import SceneKit
var viewNo = 1

class View : SCNNode {

//SCNNode contains:
//		name
//		transform, position
//		worldTransform
//		geometry
//		parentNode, childNodes

// UNUSED:
//	physics system
//	audioPlayers

	let part: Part 					// which this View represents

	init(part part_:Part) {
		part = part_
		super.init()
		name				= fmt("V%04d", viewNo)
		viewNo				= viewNo + 1
		geometry 			= SCNCylinder(radius:0.5, height:0.25)
	}

	required init?(coder: NSCoder) {
        part = coder.decodeObject(forKey: "part") as! Part // Part([:])//
		super.init(coder: coder)
	}
    override func encode(with aCoder: NSCoder) {
        aCoder.encode(part, forKey: "part")
	}

	 ///// Placement Types:
	static let placeAxisNames = ["X", "Y", "Z"]
	static let placeTypeNames = ["?","<",">","c"]

	struct PlaceType {
		let aAxis 	: Int
		let  sign 	: Bool
		let viaLink	: Bool
		let bAlig 	: Float				// 0 ==> min, 1.0 ==> max
		let cAlig 	: Float
	};

	func rePosition() {
		var myBboxMin : SCNVector3 	= SCNVector3Zero
		var myBboxMax : SCNVector3	= SCNVector3Zero


		for childNode_ in childNodes {
			if childNode_ is View {
				let childNode : View 	= childNode_ as! View

				   /// Reposition all children first
				  ///
				 /// Atoms contain only Ports inside:
				if let atom = childNode.part as? Atom {
					 // walk the View tree
					for port in childNode.childNodes {
						if let portsView = port as? View {
							atom.rePositionQ(port:portsView.part as! Port, inView:portsView)
						}
					}
				}
				 /// Normal Case
				else {
					childNode.rePosition()		// <<==== RECURSIVE
				}
				
				  /// Just STACK childNode onto side for now (Position via link soon)
				 //let gap			 	= part.simulator.gapWhenStacking as? Float
				let param :[String:Any] = childNode.part.parameters

				let aAxis 	: Int		= param["aAxis"] as? Int 	?? 0
				let  sign 	: Bool		= param["sign" ] as? Bool	?? false
				let bAlig 	: Float		= param["bAlig"] as? Float	?? 0.0
				let cAlig 	: Float		= param["cAlig"] as? Float	?? 0.0
				
				 // Place next child node (childNode) into self, along side of existing self
				let childBb				= childNode.boundingBox
				let childSize			= childBb.max - childBb.min
				let childCemter			= childBb.max + childBb.min
				let selfSize			=   myBboxMax -   myBboxMin
				let selfCenter			=   myBboxMax +   myBboxMin
				let slop				= (selfSize - childSize)/2.0
				var positionV			= selfCenter - childCemter	// center childNode in self

				//  		+--------------------+  self
				//					+----+  child
				 // Stack child in aAxis onto either side of self in axis A. Axis B and C split the slop
				let (a, b, c)			= (aAxis, (aAxis+1)%3, (aAxis+2)%3)
				let signX :Float		= sign ? -1.0 : 1.0
				positionV.set(a, toValue:positionV.component(a) + (selfSize + childSize).component(a) * signX)
				positionV.set(b, toValue:positionV.component(b) + slop.component(b) * bAlig)
				positionV.set(c, toValue:positionV.component(c) + slop.component(b) * cAlig)
				if !sign {		// false is add:
					myBboxMax.set(a, toValue:myBboxMax.component(a) + childSize.component(a)) }
				else {			// true is subtract
					myBboxMin.set(a, toValue:myBboxMin.component(a) - childSize.component(a)) }

				childNode.position 		= positionV

				print("\(childNode.name ?? "???") ", "\(childNode) :(", aAxis, sign, bAlig, cAlig, ") ", terminator:"")
				print(fmt("Bnds[%@ < %@] ", childNode.boundingBox.min.pp(), childNode.boundingBox.max.pp()))

				print(fmt("  Set childNode.posn=%@ ", positionV.pp()), terminator:"")
				print(fmt(" selfBnds [%@ < %@] )", myBboxMin.pp(), myBboxMax.pp()))
			}
		}
		__setBoundingBoxMin(&myBboxMin, max:&myBboxMax);
	}

//	func draw(_ dirtyRect: CGRect) {
//		let rectangle   :CGRect	= CGRect(x:100, y:100, width:30, height:30)
//
//		let path = UIBezierPath()
//		path.lineWidth		= 6.0
//		let o				= rectangle.origin
//		path.move(   to: CGPoint(x:o.x, y:o.y - 10))
//		path.addLine(to: CGPoint(x:o.x, y:o.y + 10 + 30 + rectangle.size.height))
//		path.stroke()
//	}
}

extension SCNNode {

	// Brian: I would love for this to be the default print routine for LLDB !!

	func pp() -> String {
		var node 		 	= self.parent
		var rv 				= "/" + (name ?? "<nil>")
		while node != nil {
			let nodeName 	= node!.name ?? "<nil>"
			rv 				= "/" + nodeName + rv
			node 			= node!.parent
		}
		rv					+= fmt(" p=\(position)\n")
		for child in childNodes {
			if let cv = child as? View {
				rv 			+= cv.pp()
			}
		}
		return rv
	}
}
